﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScript : MonoBehaviour
{
    [SerializeField]
    private Button savebut;
    [SerializeField]
    private Toggle terms;


    void Start()
    {
        savebut.interactable = false;
        terms.onValueChanged.AddListener(ValueChanged);
    }
    void ValueChanged(bool change)
    {
        savebut.interactable = terms.isOn;

    }
}
