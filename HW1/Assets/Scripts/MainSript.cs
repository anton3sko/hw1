﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com;

public class MainSript : MonoBehaviour
{
    [SerializeField]
    private Button _save;
    
    private bool showPopUp = false;

    void Start()
    {

        _save.onClick.AddListener(save);


    }

    void save()
    {
        Debug.Log("Saved");

        showPopUp = true;
    }


    void OnGUI()
    {
        if (showPopUp)
        {
            GUI.Window(0, new Rect((Screen.width / 2) - 150, (Screen.height / 2) - 75
                   , 300, 250), ShowGUI, "Saved");

        }
    }

    void ShowGUI(int windowID)
    {


        GUI.Label(new Rect(65, 40, 200, 40), "Your form has been saved");

        if (GUI.Button(new Rect(50, 150, 75, 30), "OK"))
        {
            showPopUp = false;

        }

    }
}

