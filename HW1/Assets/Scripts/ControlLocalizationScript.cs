﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com;
public class ControlLocalizationScript : MonoBehaviour
{
    [SerializeField]
    private Button _ruBut;
    [SerializeField]
    private Button _enBut;


    void Start()
    {
        Addlisteners();

    }
    private void Addlisteners()
    {

        _ruBut.onClick.AddListener(OnClickRu);
        _enBut.onClick.AddListener(OnClickEn);

    }

    private void OnClickRu()
    {
        EventManager.DispatchEvent(EventManager.RUSSIAN, new EventDataMouseClick("russian", 5));
    }
    private void OnClickEn()
    {
        EventManager.DispatchEvent(EventManager.ENGLISH, new EventDataMouseClick("english", 6));
    }


}

