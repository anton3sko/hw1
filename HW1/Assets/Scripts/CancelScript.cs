﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CancelScript : MonoBehaviour
{
    [SerializeField]
    private Button _cancel;
    [SerializeField]
    private InputField _titleInput;
    [SerializeField]
    private InputField _descriptionInput;
    [SerializeField]
    private InputField _effortInput;
    [SerializeField]
    private Dropdown _dropd;


    void Start()
    {
        _cancel.onClick.AddListener(cancel);
    }

    void cancel()
    {
        Debug.Log("Canceled");
        _titleInput.text = "Payment portal integration";
        _descriptionInput.text = "Integrate credit card and Pay Pal payments";
        _effortInput.text = "Input your effort";
        _dropd.value = 0;
    }

}


