﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com;

public class LocalizationScript : MonoBehaviour
{
    [SerializeField]
    private InputField in_title;
    [SerializeField]
    private InputField in_description;
    [SerializeField]
    private InputField in_effort;
    [SerializeField]
    private Dropdown dropd;
    [SerializeField]
    private Text title_t;
    [SerializeField]
    private Text type_t;
    [SerializeField]
    private Text description_t;
    [SerializeField]
    private Text effort_t;
    [SerializeField]
    private Button ru_but;
    [SerializeField]
    private Button en_but;
    [SerializeField]
    private Text save;
    [SerializeField]
    private Text cancel;
    [SerializeField]
    private Text check;
    void Start()
    {
        EventManager.AddListener(EventManager.RUSSIAN, OnClickRU);
        EventManager.AddListener(EventManager.ENGLISH, OnClickEn);

    }
    private void OnClickRU(EventData eventData)
    {
        EventDataMouseClick mc = (EventDataMouseClick)eventData;
        Debug.Log("It works!" + mc.Data + mc.MyInt.ToString());

        title_t.text = "Заголовок";
        type_t.text = "Тип";
        description_t.text = "Описание";
        effort_t.text = "Цена";
        in_title.text = "Интеграция платежного портала";
        in_description.text = "Интегрируйте платежы кредитной картой и Pay Pal";
        in_effort.text = "Введите вашу цену";
        save.text = "Сохранить";
        cancel.text = "Отмена";
        check.text = "Условия приняты";
        in_description.text = dropd.options[dropd.value].text;
    }
    private void OnClickEn(EventData eventData)
    {
        title_t.text = "Title";
        type_t.text = "Type";
        description_t.text = "Description";
        effort_t.text = "Effort";
        in_title.text = "Payment portal integration";
        in_description.text = "Integrate credit card and Pay Pal payments";
        in_effort.text = "Input your effort";
        save.text = "Save";
        cancel.text = "Cancel";
        check.text = "Terms accepted";
        in_description.text = dropd.options[dropd.value].text;

    }
    void Update()
    {
        in_description.text = dropd.options[dropd.value].text;
    }
}
